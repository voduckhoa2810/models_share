<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ranking extends Model
{
    protected $table    = 'ranking';
    protected $fillable =   [
        'user_id',
        'total_score'
    ];

    public $timestamps  = false;
}
